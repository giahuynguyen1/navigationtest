﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Navigation_19062021
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage =new NavigationPage(new ControlPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
