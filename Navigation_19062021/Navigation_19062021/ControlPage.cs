﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Navigation_19062021
{
    public class ControlPage : ContentPage
    {
        private Button _button;
        public ControlPage()
        {
            this.Title= "ControlPage";

            StackLayout stackLayout = new StackLayout();

            _button = new Button();
            _button.Text = "Next";
            _button.Clicked += button_Clicked;

            Content = stackLayout;
            

        }

        private async void button_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page1());
            
        }
    }
}